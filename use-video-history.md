# Video history

By default PeerTube is storing an history of the videos you have watched, a setting you can disable anytime in `My Library` > `History` on the left menu. Nothing is done with this data except for the following usages, and you can delete the history anytime via the button placed on the right of the history list.

## Catching up where you left off watching a video

Once you have begun watching a video, it will get into your history. But more than that, if you leave the video before the end, the history will remember at what time so that you can resume watching it the next time.

The feature is represented by a coloured bar below the video miniature.

![The video has been watched halfway, an orange bar represents it](/assets/video-history-miniature-bar.png)
