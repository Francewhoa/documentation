# Sharing a video

To share a video, you first have to go to the video page you want to share. If you are the publisher, you can list all your videos via the left-menu three dots option _My Videos_. Once on the video page, you just have to click the _Share_ button, and are presented with a few options:

![Modal presenting options to share a video](/assets/video-share-modal.png)

1.  a URL to the video, i.e.: `https://framatube.org/videos/watch/9db9f3f1-9b54-44ed-9e91-461d262d2205`. This address can be sent to your contact the way you want ; they will be able to access the video directly.
1.  an embed code that allows you to insert a video player in your website.

**Tip** : the small icones at the end of the line allow you to copy the whole URL at once.
